%{
/*
 * A parser implemented using BISON for the proposed language in Assn 1B.
 * Authors: Ron Thomas, Praveen Kumar Madhanagopal, Immanuel Rajkumar Philip karunakaran.
 */
 
 //Praveen - 4.	All variables must be declared with a particular type.
 //Praveen - 5.	Each variables may only be declared once.
 //Praveen - 8.	Only integer variables may be assigned the result of readInt.
 //Praveen - 12.The literal numbers 0 through 2147483647 are integer constants. Numbers outside of that range should be flagged as illegal.(completed)

#include "symbol_table.h"

%}

%code requires {
     /* Enumerations for Operators */
     typedef enum {
         mul, divide, mod, add, sub, o_undef
     } Operator;

     /* Enumerations for Relational Operators */
     typedef enum {
         eq, neq, lt, gt, le, ge, ro_undef
     } RelationalOperator;
     
     /* Enumerations for Node Type */
     typedef enum {
         programNode, declarationsNode, stmtSeqNode, typeNode, stmtNode, assignmentNode, 
         expressionNode, simpleExprNode, termNode, factorNode, ifStmtNode, elseNode, whileNode, writeIntStmtNode
     } NodeType;

     /* FrontLine declarations of nodes - Definition follows*/
     typedef struct Node Node; 
     typedef struct ProgramNode ProgramNode;
     typedef struct DeclarationsNode DeclarationsNode;
     typedef struct StatementSeqNode StatementSeqNode;
     typedef struct TypeNode TypeNode;
     typedef struct StatementNode StatementNode;
     typedef struct AssignmentNode AssignmentNode;
     typedef struct IfStatementNode IfStatementNode;
     typedef struct ElseNode ElseNode;
     typedef struct WhileNode WhileNode;
     typedef struct WriteIntNode writeIntNode;
     typedef struct ExpressionNode ExpressionNode;
     typedef struct SimpleExprNode SimpleExprNode;
     typedef struct TermNode TermNode;
     
     

     struct ProgramNode {
        struct Node * declarations;
        struct Node * stmtSeq;
     };

     struct DeclarationsNode {
        char * ident;
        struct Node * type;
        struct Node * declarations;
        int empty;
     };

     struct StatementSeqNode {
        struct Node * stmt;
        struct Node * stmtSeq;
     };

     struct StatementNode {
        struct Node * assnStmt;
        struct Node * ifStmt;
        struct Node * whileStmt;
        struct Node * writeIntStmt;
     };

     struct AssignmentNode {
        char * ident;
        int readIntVal;
        struct Node * expr;
     };

     struct IfStatementNode {
        struct Node * expr;
        struct Node * stmtSeq;
        struct Node * elseClause;
     };

     struct ElseNode {
         struct Node * stmtSeq;
     };

     struct WhileNode {
         struct Node * expr;
         struct Node * stmtSeq;
     };

     struct WriteIntNode {
         struct Node * expr;
     };

     struct TypeNode {
        char * type;
     };

     struct ExpressionNode {
         struct Node * simpleExprL;
         RelationalOperator roptr;
         struct Node * simpleExprR;
     };

     struct SimpleExprNode {
         struct Node * termL;
         Operator optr;
         struct Node * termR;
     };

     struct TermNode {
         struct Node * factorL;
         Operator optr;
         struct Node * factorR;
     };

     struct FactorNode {
         char * ident;
         int number;
         int boollit;
         struct Node * expr;
         int type;
     };

     /* Parse Tree Node Structure */
     typedef struct Node {
         NodeType nodeType;
         int line_number;
         union {
            struct ProgramNode programNode;
            struct DeclarationsNode declarationsNode;
            struct StatementSeqNode statementSeqNode;
            struct TypeNode typeNode;
            struct StatementNode statementNode;
            struct AssignmentNode assignmentNode;
            struct IfStatementNode ifStatementNode;
            struct ElseNode elseNode;
            struct WhileNode whileNode;
            struct WriteIntNode writeIntNode;
            struct ExpressionNode expressionNode;
            struct SimpleExprNode simpleExprNode;
            struct TermNode termNode;
            struct FactorNode factorNode;
         } body;
     } Node;

     /* Routines make_<NODETYPE> creates parseTree nodes and returns the pointer to the created node */
     static Node * make_term ( Node * factorL, Operator op, Node * factorR, int type, int line_no ) {
        Node * node;
        if( (node = (Node *) malloc( sizeof(Node) )) == NULL ) 
		printf("\nError Allocating memory");
        node->nodeType = termNode;
        node->body.termNode.factorL = factorL;
        node->body.termNode.factorR = factorR;
        node->body.termNode.optr = op;
        node->line_number = line_no;
        return node;
     }

     static Node * make_factor ( char * id, int n, int blit, Node * ex, int type, int line_no  ) {
        Node * node;
        if( (node = (Node *) malloc( sizeof(Node) )) == NULL ) 
		printf("\nError Allocating memory");
        node->nodeType = factorNode;
        node->body.factorNode.type = type;
        if( type == 1 ) {
            node->body.factorNode.ident = malloc ( strlen(id) );
            strncpy(node->body.factorNode.ident , id, strlen(id));
        }
	else if ( type == 2 ){ 
	    //printf("\nmake_number = %d", n);
            node->body.factorNode.number = n;
        }
        else if ( type == 3 ){ 
            node->body.factorNode.boollit = blit;
        }
        else if ( type == 4 ){
            node->body.factorNode.expr = ex;
        }
         node->line_number = line_no;
        return node;
     }

     static Node * make_simpleExpr ( Node * termL , Operator op, Node * termR, int type, int line_no) {
        Node * node;
        if( (node = (Node *) malloc( sizeof(Node) )) == NULL ) 
		printf("\nError Allocating memory");
        node->nodeType = simpleExprNode;
        node->body.simpleExprNode.termL = termL;
        node->body.simpleExprNode.termR = termR;
        node->body.simpleExprNode.optr = op;
        node->line_number = line_no;
        return node;
     }

     static Node * make_expr ( Node * simpExprL, RelationalOperator op, Node * simpExprR, int type, int line_no) {
        Node * node;
        if( (node = (Node *) malloc( sizeof(Node) )) == NULL ) 
		printf("\nError Allocating memory");
        node->nodeType = expressionNode;
	node->body.expressionNode.simpleExprL = simpExprL;
        node->body.expressionNode.simpleExprR = simpExprR;
        node->body.expressionNode.roptr = op;
        node->line_number = line_no;
        return node;
     }
    
     static Node * make_assignment ( char * id, int val, Node * ex, int type, int line_no) {
        Node * node;
        if( (node = (Node *) malloc( sizeof(Node) )) == NULL ) 
		printf("\nError Allocating memory");
        node->nodeType = assignmentNode;
        node->body.assignmentNode.ident = malloc ( strlen(id) );
        strncpy(node->body.assignmentNode.ident , id, strlen(id));
        if( type == 1 ) {
            node->body.assignmentNode.readIntVal = val;
            node->body.assignmentNode.expr = NULL;
        } else if ( type == 2 ) {
            node->body.assignmentNode.expr = ex;
        }
         node->line_number = line_no;
          //printf("\nLine number at MAKE ASSIGNMENT is %d and %s",line_no,id);
        return node;
     }

     static Node * make_ifStatement ( Node * ex, Node * stSeq, Node * eClause, int line_no) {
        Node * node;
        if( (node = (Node *) malloc( sizeof(Node) )) == NULL ) 
		printf("\nError Allocating memory");
        node->nodeType = ifStmtNode;
        node->body.ifStatementNode.expr = ex;
        node->body.ifStatementNode.stmtSeq = stSeq;
        node->body.ifStatementNode.elseClause = eClause;
         node->line_number = line_no;
         //printf("\nLine number at IF STATEMENT is %d",line_no);
        return node;
     }

     static Node * make_elseStatement ( Node * stSeq, int line_no  ) {
        Node * node;
        if( (node = (Node *) malloc( sizeof(Node) )) == NULL ) 
		printf("\nError Allocating memory");
        node->nodeType = elseNode;
        node->body.elseNode.stmtSeq = stSeq;
         node->line_number = line_no;
        return node;
     }

     static Node * make_whileStatement ( Node * ex, Node * stSeq, int line_no  ) {
        Node * node;
         //printf("Line number at WHILE STATEMENT is %d\n",line_no);
        if( (node = (Node *) malloc( sizeof(Node) )) == NULL ) 
		printf("\nError Allocating memory");
        node->nodeType = whileNode;
        node->body.whileNode.expr = ex;
        node->body.whileNode.stmtSeq = stSeq;
         node->line_number = line_no;
         
        return node;
     }

     static Node * make_writeInt ( Node * ex, int line_no  ) {
        Node * node;
        if( (node = (Node *) malloc( sizeof(Node) )) == NULL ) 
		printf("\nError Allocating memory");
        node->nodeType = writeIntStmtNode;
        node->body.writeIntNode.expr = ex;
         node->line_number = line_no;
        return node;
     }

     static Node * make_type ( char * t, int line_no  ) {
        Node * node = (Node *) malloc( sizeof(Node) );
        node->nodeType = typeNode;
        node->body.typeNode.type = malloc ( strlen(t) );
        strncpy(node->body.typeNode.type , t, strlen(t));
        node->line_number = line_no;
        return node;
     }

     static Node * make_stmt ( Node * sNode , int type, int line_no  ) {
        Node * node = (Node *) malloc( sizeof(Node) );
        node->nodeType = stmtNode;
        if( type == 1 )
            node->body.statementNode.assnStmt = sNode;
        else if( type == 2 )
            node->body.statementNode.ifStmt = sNode;
        else if( type == 3 )
            node->body.statementNode.whileStmt = sNode;
        else if( type == 4 )
            node->body.statementNode.writeIntStmt = sNode;
            
         node->line_number = line_no;
        return node;
     }

     static Node * make_stmtSeq ( Node * st, Node * stSeq, int line_no  ) {
        Node * node = (Node *) malloc( sizeof(Node) );
        node->nodeType = stmtSeqNode;
        node->body.statementSeqNode.stmt = st;
        node->body.statementSeqNode.stmtSeq = stSeq;
         node->line_number = line_no;
        return node;
     }

     static Node * make_declarations ( char * id, Node * t, Node * dec, int empty, int line_no  ) {
        Node * node = (Node *) malloc( sizeof(Node) );
        node->nodeType = declarationsNode;
        node->body.declarationsNode.type = t;
        node->body.declarationsNode.declarations = dec;
        node->body.declarationsNode.ident = malloc ( strlen(id) );
        strncpy(node->body.declarationsNode.ident , id, strlen(id));
        node->body.declarationsNode.empty = empty;
         node->line_number = line_no;
        return node;
     }

     static Node * make_program ( Node * dec, Node * stSeq, int line_no  ) {
        Node * node = (Node *) malloc( sizeof(Node) );
        node->nodeType = programNode;
        node->body.programNode.declarations = dec;
        node->body.programNode.stmtSeq = stSeq;
         node->line_number = line_no;
          //printf("\nLine number at PROGRAM STATEMENT is %d",line_no);
        return node;
     }
     
     static void printoptr(Operator op){
	switch (op){
         case mul:
         	printf(" *");
         	break;
         case divide:
         	printf(" /");
         	break;
         case mod:
         	printf(" %%");
         	break;
         case add:
         	printf(" +");
         	break;
         case sub:
         	printf(" -");
         	break;
         }
     }
     
     static void printroptr(RelationalOperator op){
	switch (op){
         case eq:
         	printf(" =");
         	break;
         case neq:
         	printf(" !=");
         	break;
         case lt:
         	printf(" <");
         	break;
         case gt:
         	printf(" >");
         	break;
         case le:
         	printf(" <=");
         	break;
         case ge:
         	printf(" >=");
         	break;
         }
     }

     
     typedef enum {
         TYPE_INT, TYPE_BOOL, TYPE_UNDEF, TYPE_ANY
     } ReturnType;

     /* Code Generator Module */
     static ReturnType generateCode ( Node * n, int level ) {
        if( n != NULL ) {
             int i=0;
             ReturnType returnType;
             ReturnType lExprType, rExprType;
             switch ( n->nodeType ) {
                 case programNode : { 
                     printf("#include<stdio.h>\n");
                     printf("typedef int bool;\n");
		     printf("#define true 1\n");
	             printf("#define false 0\n");
                     printf("void main() {");
                     generateCode(n->body.programNode.declarations, level+1);
                     printf("\n");
                     generateCode(n->body.programNode.stmtSeq, level+1);
                     printf("}");
                     return TYPE_ANY;
                 }
                 case declarationsNode : { 
                     generateCode(n->body.declarationsNode.type, level+1);                     
                     if(! (n->body.declarationsNode.empty) ){
                     	printf("%s;", n->body.declarationsNode.ident);
                     }
                     generateCode(n->body.declarationsNode.declarations, level+1);
                     return TYPE_ANY;
                 }
                 case stmtSeqNode : { 
                     generateCode(n->body.statementSeqNode.stmt, level+1);
                     if( n->body.statementSeqNode.stmt != NULL)
                     	printf("\n");
                     generateCode(n->body.statementSeqNode.stmtSeq, level+1);
                     return TYPE_ANY;
                 }
                 case typeNode : { 
                     printf("\n%s ", n->body.typeNode.type);
                     if( n->body.typeNode.type == "INT" )
                         return TYPE_INT;
                     else if( n->body.typeNode.type == "BOOL" )
                         return TYPE_BOOL;
                     else 
			 return TYPE_UNDEF;
                 }
                 case assignmentNode : { 
                     if(n->body.assignmentNode.expr == NULL){
                     	 printf("scanf(\"%%d\", &%s )", n->body.assignmentNode.ident);
                     }
                     else{
                     	printf("%s =", n->body.assignmentNode.ident);
                     }
                     rExprType = generateCode(n->body.assignmentNode.expr, level+1);
                     
                     int i = check_data_type(n->body.assignmentNode.ident);
                     
                     if( rExprType == TYPE_BOOL ) {
                         //Check if the data type of identifier is also bool. If not report error
			 if(i == 0){
			 	printf("\n*****ERROR: Line_number: %d : Variable not Declared*****p1", n->line_number);
			 }
			 else if(i != 2){
			 	printf("\n*****ERROR: Line_number: %d : Mismatch Data type,Assigning to NON BOOL TYPE Variable*****", n->line_number);
			 }
                     } else if( rExprType == TYPE_INT ) {
                         //Check if the data type of identifier is also int. If not report error
                         if(i == 0){
			 	printf("\n*****ERROR: Line_number: %d : Variable not Declared*****p2", n->line_number);
			 }
			 else if(i != 1){
			 	printf("\n*****ERROR: Line_number: %d : Mismatch Data type,Assigning to NON INT TYPE Variable*****", n->line_number);
			 }
                     } else {
                         //Report Error
                         printf("\n*****ERROR: Line number: %d : Undefined Error*****", n->line_number);
                     }
                     return TYPE_ANY; 
                 }
                 case ifStmtNode : {
                     printf("if(");
                     rExprType = generateCode(n->body.ifStatementNode.expr, level+1);
                     printf(") {\n");
                     generateCode(n->body.ifStatementNode.stmtSeq, level+1);
                     printf("}");
                     generateCode(n->body.ifStatementNode.elseClause, level+1);
                     if( rExprType == TYPE_BOOL ) {
                         //printf("\n//If statement Check condition evaluates to a bool");
                         return TYPE_ANY;
                     } else {
                         printf("\n*****ERROR : Line number: %d :If statement Check condition does not evaluates to a bool*****", n->line_number);
                         return TYPE_UNDEF;
	             }
                 }
                 case elseNode : {
                     if(n->body.elseNode.stmtSeq != NULL)
                     	printf("\nelse {");
                     generateCode(n->body.elseNode.stmtSeq, level+1);
                     if(n->body.elseNode.stmtSeq != NULL)
                     	printf("\n }");
                     return TYPE_ANY;
                 }
                 case whileNode : {
                     printf("while(");
                     rExprType = generateCode(n->body.whileNode.expr, level+1);
                     printf(") { \n");
                     generateCode(n->body.whileNode.stmtSeq, level+1);
                     printf("}");
                     if( rExprType == TYPE_BOOL ) {
                         //printf("\n//While statement Check condition evaluates to a bool");
                         return TYPE_ANY;
                     } else {
                         printf("\n*****ERROR : Line number: %d :While statement Check condition does not evaluates to a bool*****", n->line_number);
                         return TYPE_UNDEF;
	             }
                 }
                 case writeIntStmtNode : {
                     printf("printf( \"%%d\\n\", ");
                     rExprType = generateCode(n->body.writeIntNode.expr, level+1);
                     printf(")");
                     if( rExprType == TYPE_INT ) {
                         //printf("\n//Right Side of the Write Int is an Ineteger");
                         return TYPE_ANY;
                     } else {
                         printf("\n*****ERROR : Line number: %d :Right Side of the Write Int is not an Ineteger*****", n->line_number);
                         return TYPE_UNDEF;
	             }
                 }
                 case stmtNode : {
                     //Need to change the code to call only those that are not NULL - PRAVEEN
                     generateCode(n->body.statementNode.assnStmt, level+1);
                     if(n->body.statementNode.assnStmt != NULL ) {
                     	printf(";");
                     }
                     generateCode(n->body.statementNode.ifStmt, level+1);
                     generateCode(n->body.statementNode.whileStmt, level+1);
                     generateCode(n->body.statementNode.writeIntStmt, level+1);
                     if(n->body.statementNode.writeIntStmt != NULL )
                     	printf(";");
                     return TYPE_ANY;
                 }
                 case expressionNode : { 
                     lExprType = generateCode(n->body.expressionNode.simpleExprL, level+1);
                     if(n->body.expressionNode.roptr != ro_undef){
                      	 printroptr(n->body.expressionNode.roptr);
                         rExprType = generateCode(n->body.expressionNode.simpleExprR, level+1);
                         if( lExprType == TYPE_INT && rExprType == TYPE_INT ) {
                             //printf("\n//Both the left and right operands are of %d is of type INT",n->body.expressionNode.roptr);
                             return TYPE_BOOL;
                         } else {
                             printf("\n*****ERROR : Line number: %d :Unmatched Return Types for Relational Operator*****", n->line_number);
                             return TYPE_UNDEF;
                         }
                     }
                     return lExprType;
                 }
                 case simpleExprNode : { 
                     lExprType = generateCode(n->body.simpleExprNode.termL, level+1);
                     if(n->body.simpleExprNode.optr != o_undef){
                         printoptr(n->body.simpleExprNode.optr);
                         rExprType = generateCode(n->body.simpleExprNode.termR, level+1);
                         //Both lExprType and rExprType must be integers for any optr
                         if( lExprType == TYPE_INT && rExprType == TYPE_INT ) {
                             //printf("\n//Both the left and right operands of SimpleExpr Optr are of type INT");
                             return TYPE_INT;
                         }
                         else if( lExprType != TYPE_INT ) {
                             printf("\n*****ERROR : Line number: %d :Left operands of SimpleExpr Optr are not of type INT*****", n->line_number);
                             return TYPE_UNDEF;
                         }
                         else if( rExprType != TYPE_INT ) {
                             printf("\n*****ERROR : Line number: %d :Right operands of Simple Expr Optr are not of type INT*****", n->line_number);
                             return TYPE_UNDEF;
                         }
                     }
                     return lExprType;
                 }
                 case termNode : { 
                     lExprType = generateCode(n->body.termNode.factorL, level+1);
                     int div = 0;
                     if(n->body.termNode.optr != o_undef){
                     	 if(n->body.termNode.optr == divide){
                     	 	printf("\nDivision Operator");
                     	 	div = 1;
                     	 }
                         printoptr(n->body.termNode.optr);
                         rExprType = generateCode(n->body.termNode.factorR, level+1); 
                         
                         //Both lExprType and rExprType must be integers for any optr
                         if( lExprType == TYPE_INT && rExprType == TYPE_INT ) {
                             //printf("\n//Both the left and right operands of Term Node are of type INT");
                             if(div){
                         	
                         	if(n->body.termNode.factorR->body.factorNode.ident)
                         	printf( "\nif(%s == 0 ) { printf(\"ERROR : Line Number :%d :Divide by zero ident\"); exit(0);}", n->body.termNode.factorR->body.factorNode.ident, n->line_number);
                         	else if(n->body.termNode.factorR->body.factorNode.number == 0)
                         		printf("\nERROR : Line Number :%d :Divide by zero number", n->line_number);
                         	
                             }
                             return TYPE_INT;
                         }
                         else if( lExprType != TYPE_INT ) {
                             printf("\n*****ERROR : Line number: %d :Left operands of Term Node are not of type INT*****", n->line_number);
                             return TYPE_UNDEF;
                         }
                         else if( rExprType != TYPE_INT ) {
                             printf("\n*****ERROR : Line number: %d :Right operands of Term Node are not of type INT*****", n->line_number);
                             return TYPE_UNDEF;
                         }
                     }
                     return lExprType;
                 }
                 case factorNode : { 
                     switch(n->body.factorNode.type) {
                         case 1: 
                     	     printf(" %s", n->body.factorNode.ident);
                             //TODO: We have the ident here. So check the symbol table and get the dataType associated with this identifier.
                             // Error 1: If dataType not present - Illegal use of identifier before the usage - Report
                             // If the returned type from symbol table is integer, return TYPE_INT, if bool, return TYPE_BOOL
                             // Assuming the return is int:
                            // printf("\n//Factor Node of type INT identifier");
                             //printf("\n//Factor Node of type BOOL identifier");
                             switch(check_data_type(n->body.factorNode.ident)){
                             	case 0:
                             		printf("\n*****ERROR : Line number: %d :Variable is not declared*****", n->line_number);
                             		break;
                             	case 1:
                             		return TYPE_INT;
                             		break;
                             	case 2:
                             		return TYPE_BOOL;
                             		break;
                             	default:
                             		printf("\n*****ERROR : Line number: %d :Undefined Error in Factor Node*****", n->line_number);
                             		return TYPE_UNDEF;
                             	}
                      
                         case 2:
                     	     printf(" %d", n->body.factorNode.number);
                             //printf("\n//Factor Node of Number");
                             return TYPE_INT;
                         case 3:
                     	     if(n->body.factorNode.boollit == 0) {
                     	 	printf("false");
                                //printf("\n//Factor Node of boolean False");
                                return TYPE_BOOL;
                             }
                     	     else {
                     	 	printf("true");
                                //printf("\n//Factor Node of boolean True");
                                return TYPE_BOOL;
                             }
                         case 4:
                             printf(" (");
                             lExprType = generateCode(n->body.factorNode.expr, level+1);
                             printf(" )");
                             return lExprType;
                         default:
                     	     printf("Undefined Factor");
                             return TYPE_UNDEF; 
                     }
                 }
                 default : { printf("Undefined Node Type"); break; }
             }
        }
        return 0;
    }
}

%union
{
    char * str_val;
    int int_val;
    Operator optr;
    RelationalOperator roptr;
    Node * nodePtr;
};

/* TOKENS */
%token COMMENT
%token WHITESPACE  
%token <int_val> NUMBER
%token <int_val> BOOLLIT
%token <str_val> IDENTIFIER
%token IF
%token THEN
%token ELSE
%token BEGIN1	
%token END
%token WHILE
%token DO
%token PROGRAM
%token VAR
%token AS
%token INT
%token BOOL
%token WRITEINT     
%token READINT      
%token LP             
%token RP         
%token ASGN                    
%token SC
%token <optr> OP2
%token <optr> OP3
%token <roptr> OP4

%start program

%type <nodePtr> program declarations statementSequence type statement assignment expression simpleExpression term factor
%type <nodePtr> ifStatement elseClause whileStatement writeInt
%%

/* Grammar Declarations */
program:
	PROGRAM declarations BEGIN1 statementSequence END 
        {
      	    //printf("void main()\t{ \n");
            $$ = make_program( $2, $4,line_counter );
            printf("\nCode Conversion\n");
            generateCode($$, 0);
            printf("\n");
            //printf("\n}\n");

        }
	;

declarations:
	VAR IDENTIFIER AS type SC declarations 
        {
            /* TODO : Need to check if this variable already declared from symbol table entry - report error if so */
            $$ = make_declarations( $2, $4, $6, 0,line_counter );
            //printf("Dec : %s %s; \n", $4->body.declarationsNode.ident, $2);
            
            add_data_type($2, $4->body.declarationsNode.ident);
            //print_users();

        }
        |  { $$ = make_declarations( "", NULL, NULL, 1,line_counter); }
        ;

type:
	 INT 					{ $$ = make_type("int",line_counter); } 
	
         | BOOL 				{ $$ = make_type("bool",line_counter); }
	 ;

statementSequence:
	statement SC statementSequence		{ $$ = make_stmtSeq($1, $3,line_counter); }
        | /* Empty */				{ $$ = make_stmtSeq(NULL, NULL,line_counter); }
        ;

statement: 
	 assignment				{ $$ = make_stmt($1, 1,line_counter); }
        | ifStatement 				{ $$ = make_stmt($1, 2,line_counter); }
        | whileStatement 			{ $$ = make_stmt($1, 3,line_counter); }
        | writeInt				{ $$ = make_stmt($1, 4,line_counter); }
        ;

assignment:
	IDENTIFIER ASGN expression 
        {
            /* TODO: 1. Need to check if the $1 is already declared - if not report error */
            $$ = make_assignment ($1, 0, $3, 2,line_counter);
        }
        | IDENTIFIER ASGN READINT  
        {
            /* TODO: 1. Check 1 from above
                     2. Read from console the input number - If INT assign it to id and update symTable - else report error input */
         $$ = make_assignment ($1, 10, NULL, 1,line_counter); //Need to change the value of 10 to another function call
            //printf("IDENTIFIER : %s\n", $1);
           switch(check_data_type($1)){
                             	case 0:
                             		printf("\n*****ERROR : Line number: %d :Variable is not declared*****", line_counter);
                             		break;
                             	case 1:
                             		
                             	case 2:
                             		printf("\n*****ERROR : Line number: %d :Variable is not Int*****", line_counter);
                             		break;
                             	default:
                             		printf("\n*****ERROR : Line number: %d :Undefined Error in assignment*****", line_counter);
                             		break;
                             		return TYPE_UNDEF;
                             	}
            //printf("%s = readInt; \n", $1);
        }
        ;

ifStatement:
	IF expression THEN statementSequence elseClause END 		
	{	//printf("%s %s %s,", $2->body.ifStatementNode.expr->, $4, $5);
	 	$$ = make_ifStatement($2, $4, $5,line_counter ); 
	 	
	 	 
	 }
	;

elseClause:
	ELSE statementSequence 			{ $$ = make_elseStatement($2,line_counter); }
        | /* empty */ 				{ $$ = make_elseStatement(NULL,line_counter); }
        ;

whileStatement:
	WHILE expression DO statementSequence END 			{ $$ = make_whileStatement($2, $4,line_counter);
	//printf("Line number at WHile Statement is %d\n",line_counter); 
	}
	;

writeInt:
	WRITEINT expression 			{ $$ = make_writeInt($2,line_counter); }
	;

expression:
        simpleExpression 
        {
            RelationalOperator temp = ro_undef;
            $$ = make_expr($1, temp, NULL, 1,line_counter);
        }
        | simpleExpression OP4 simpleExpression	{ $$ = make_expr( $1, $2, $3, 2,line_counter ); }
        ;

simpleExpression:
         term
         {   
             Operator temp = o_undef;
             $$ = make_simpleExpr( $1, temp, NULL, 1,line_counter );
         }
	 | term OP3 term 
         {
            /* TODO: Operations are on 2s compliment for negative numbers. Perform as reqd. */
            $$ = make_simpleExpr( $1, $2, $3, 2,line_counter );
         }
         ;

term:
        factor
        {
            Operator temp = o_undef;
            $$ = make_term ( $1, temp, NULL, 1,line_counter );
        }
	| factor OP2 factor 
        {
        /* TODO: Operations are on 2s compliment for negative numbers. Perform as reqd. */
        /* TODO: 1. check if both factors are numbers */
            $$ = make_term ( $1, $2, $3, 2,line_counter );
        }
        ;

factor:
	IDENTIFIER 			{ $$ = make_factor( $1, 0, 0, NULL, 1,line_counter ); }
        | NUMBER			{ $$ = make_factor( "", $1, 0, NULL, 2 ,line_counter); }
        | BOOLLIT			{ $$ = make_factor( "", 0, $1, NULL, 3,line_counter ); }
        | LP expression RP 		{ $$ = make_factor( "", 0, 0, $2, 4,line_counter); }
        ;

%%

int yyerror(char *s) {
    printf ("\nyyerror : %s Line_Number: %d\n", s, line_counter);
}

int main(void) {
    yyparse();
}


