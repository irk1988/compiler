#include "symbol_table.h"

int line_counter = 1;

struct Token symbolTable[1000];

/* Check if the symbol table has an entry already for this lexeme. If so, ignore, else add it to symbol table 
 * Uses linear search - Optimization: Can be made to use hash functions to perform better lookups.
 */ 
int add_user( char * lexeme ) {
	int i=0, flag=0;
	for( i=0; i< stable_size; i++ )
		if( strcmp( symbolTable[i].lexeme, lexeme ) == 0 ) {
			//skip; Already present
			flag = 1;
			break;
		} 

	if( !flag ) {
	    strcpy(symbolTable[stable_size].lexeme, lexeme);
	    stable_size++;
	}
	
	//printf("add_user : **%s**\n", symbolTable[i].lexeme);
	
	return 0;
}

int check_user( char * lexeme ) {
//identifier persent or not
	int i=0, flag=0;
	for( i=0; i< stable_size; i++ )
		if( strcmp( symbolTable[i].lexeme, lexeme ) == 0 ) {
			//skip; Already present
			flag = 1;
			break;
		} 

	if( flag ) {
		return 1;
	}
	

	
	return 0;
}

int add_data_type( char * lexeme, char * dataType ) {
	int i=0, flag=0;
	for( i=0; i< stable_size; i++ )
		if( strcmp( symbolTable[i].lexeme, lexeme ) == 0 ) {
			//skip; Already present
			flag = 1;
			break;
		} 

	if( flag ) {
		strncpy(symbolTable[i].datatype, dataType, strlen(dataType));
		return 1;
	}
	
	//printf("add_datauser : **%s**\n", symbolTable[i].datatype);
	
	//error - print
	return 0;
}

//int 1 bool 2 0 not dec
int check_data_type ( char * lexeme ) {
	int i=0, flag=0;
	for( i=0; i< stable_size; i++ )
		if( strcmp( symbolTable[i].lexeme, lexeme ) == 0 ) {
			//skip; Already present
			flag = 1;
			break;
		} 

	if( flag ) {
		if( strcmp( symbolTable[i].datatype, "bool" ) == 0 ) 
			return 2;
		else if( strcmp( symbolTable[i].datatype, "int" ) == 0 ) 
			return 1;
		
	}

	//printf("\ncheck_datauser : **%s**\n", symbolTable[i].datatype);

	return 0;
}

