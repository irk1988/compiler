%{
/*
 * A scanner implemented using FLEX for the proposed language in Assn 1B.
 * The scanned tokens are added to a symbol table for future use in parsing.
 * Authors: Ron Thomas, Praveen Kumar Madhanagopal, Immanuel Rajkumar Philip karunakaran.
 */


#include "parser.tab.h"
#include "symbol_table.h"

%}

newline     [\n]
comment     (%)(.*)([\n])
whitespace  [ \t]+
number      [1-9][0-9]*|0
boollit     (true|false)
ident       [A-Z][A-Z0-9]*
if          (if)
then        (then)
else        (else)
begin       (begin)
end         (end)
while       (while)
do          (do)
program     (program)
var         (var)
as          (as)
int         (int)
bool        (bool)
writeInt    (writeInt)
readInt     (readInt)
leftparan   "("
rightparan  ")"
asgnop      ":="
semicolon   ";"
optr2       (\*|div|mod)
optr3       (\+|-)
optr4       (=|!=|<|>|<=|>=)

%%

{newline}       { 
                    line_counter++; 
                }
{comment}       return(COMMENT);
{whitespace}    { 
                }
{number}        {   long value = atol(yytext);
		    if(value < -2147483648  || value > 2147483647){
		    	//value = -1;
		    	printf("\n*****ERROR: Line_number: %d : Integer Overflow*****", line_counter);
		    }
                    yylval.int_val = (int)value;
                    return(NUMBER);
                }
{boollit}       {
                    yylval.int_val = (strcmp(yytext, "true") == 0) ? 1 : 0;
                    return(BOOLLIT);
                }
{ident}         {
                    yylval.str_val = malloc(strlen(yytext));
                    strncpy(yylval.str_val, yytext, strlen(yytext));
                    add_user(yytext);
                    //print_users();
                    return(IDENTIFIER);
                }
{if}            return(IF);
{then}          return(THEN);
{else}          return(ELSE);
{begin}         return(BEGIN1);
{end}           return(END);
{while}         return(WHILE);
{do}            return(DO);
{program}       return(PROGRAM);
{var}           return(VAR);
{as}            return(AS);
{int}           return(INT);
{bool}          return(BOOL);
{writeInt}      return(WRITEINT);
{readInt}       return(READINT);
{leftparan}     return(LP);
{rightparan}    return(RP);
{asgnop}        return(ASGN);
{semicolon}     return(SC);
{optr2}         {
                    switch(yytext[0]) {
                        case '*' : { yylval.optr = mul; break; }
                        case 'd' : { yylval.optr = divide; break; }
                        case 'm' : { yylval.optr = mod; break; }
                        default : { /*ERROR*/ }
                    }
                    return(OP2);
                }
{optr3}         {
                    switch(yytext[0]) {
                        case '+' : { yylval.optr = add; break; }
                        case '-' : { yylval.optr = sub; break; }
                        default  : { /* ERROR*/ }
                    }
                    return(OP3);
                }
{optr4}         {
                    switch(yytext[0]) {
                        case '<' : { if( (yytext[1]!='\0')&&(yytext[1]=='=') ) yylval.roptr = le; 
                                     else if( yytext[1]=='\0') yylval.roptr = lt;  break; }
                        case '>' : { if( (yytext[1]!='\0')&&(yytext[1]=='=') ) yylval.roptr = ge; 
                                     else if( yytext[1]=='\0') yylval.roptr = gt;  break; }
                        case '=' : { yylval.roptr = eq; break; }
                        case '!' : { if( yytext[1]=='=' ) yylval.roptr = neq; break; }
                        default  : { /* ERROR*/ }
                    }
                    return(OP4);
                }
                
.		{
			printf("\n*****UNDEFINED ERROR IN SCANNER*****");
		}
	

