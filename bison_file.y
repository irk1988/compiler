//declaration
%{
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
%}

// Symbols.
%union
{
	char	*sval;
	int 	int_val;
	
};

%token COMMENT
%token WHITESPACE  
%token <int_val> NUMBER
%token <int_val> BOOLLIT
%token <sval> IDENTIFIER
%token IF
%token THEN
%token ELSE
%token BEGIN1	
%token END
%token WHILE
%token DO
%token PROGRAM
%token VAR
%token AS
%token INT
%token BOOL
%token WRITEINT     
%token READINT      
%token LP             
%token RP         
%token ASGN                    
%token SC
%token <sval> OP2
%token <sval> OP3
%token <sval> OP4

%start program
%%

program:
	PROGRAM declarations BEGIN1 statementSequence END { printf("begin\n"); }
	;

declarations:
	VAR IDENTIFIER AS type SC declarations { printf("Procedure\n"); }
        | /* empty */
        ;

type:
	 INT | BOOL
	 ;

statementSequence:
	statement SC statementSequence
        | /* empty */
        ;

statement: 
	 assignment
        | ifStatement
        | whileStatement
        | writeInt
        ;

assignment:
	IDENTIFIER ASGN expression
        | IDENTIFIER ASGN READINT
        ;

ifStatement:
	IF expression THEN statementSequence elseClause END
	;

elseClause:
	ELSE statementSequence
        | /* empty */
        ;

whileStatement:
	WHILE expression DO statementSequence END
	;

writeInt:
	WRITEINT expression
	;

expression:
	simpleExpression
        | simpleExpression OP4 simpleExpression
        ;

simpleExpression:
	 term OP3 term
         | term
         ;

term:
	factor OP2 factor
        | factor
        ;

factor:
	IDENTIFIER
        | NUMBER
        | BOOLLIT
        | LP expression RP
        ;

%%

int yyerror(char *s) {
  printf ("yyerror : %s\n", s);
}

int main(void) {
  yyparse();
}

